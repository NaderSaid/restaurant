﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Restaurant
{
    /// <summary>
    /// Interaction logic for CheckOutWindow.xaml
    /// </summary>
    public partial class CheckOutWindow : Window
    {
        User user;
        float totalPrice;
        ObservableCollection<Menu> basketMenuCollection;
        ObservableCollection<Product> basketProductCollection;

        public CheckOutWindow(User user, float totalPrice, ObservableCollection<Product> basketProductCollection, ObservableCollection<Menu> basketMenuCollection)
        {
            InitializeComponent();

            this.user = user;
            this.totalPrice = totalPrice;
            if (basketMenuCollection != null)
            this.basketMenuCollection = basketMenuCollection;
            if (basketProductCollection != null) 
            this.basketProductCollection = basketProductCollection;

            UpdateValues();
        }

        void UpdateValues()
        {
            textBlock_FirstName.Text = user.FirstName;
            textBlock_LastName.Text = user.LastName;
            textBox_Address.Text = user.Address;
            textBox_PhoneNumber.Text = user.Telephone;
        }


        private void btn_PlaceOrder_Click(object sender, RoutedEventArgs e)
        {
            if (textBox_Address.Text != "" && textBox_PhoneNumber.Text.Length == 10 && (comboBox_CashOrCard.Text != "" || comboBox_CashOrCard.Text != null))
            {
                Service.InsertDelivery(int.Parse(user.Id), totalPrice, comboBox_CashOrCard.Text, textBox_PhoneNumber.Text, textBox_Address.Text);
                int lastDeliveryId = Service.LastDeliveryIdFromClientIdSelect(user.Id);
                
                foreach (Menu menu in basketMenuCollection)
                {
                    Service.InsertMenuDelivery(int.Parse(menu.Id), lastDeliveryId, 1);
                }
                
                foreach (Product product in basketProductCollection)
                {
                    Service.InsertProductDelivery(int.Parse(product.Id), lastDeliveryId, 1);
                }

                PopupWindow popupWindow = new PopupWindow("Your order has been placed !");
                popupWindow.Show();
                this.Close();
            }
            else
            {
                PopupWindow popupWindow = new PopupWindow("Error placing your order, please try again !");
                popupWindow.Show();
            }

        }
    }
}
