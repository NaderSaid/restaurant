﻿using System;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data.SqlClient;

namespace Restaurant
{
    static public class Service
    {
        static public bool VerifyUser(string email, string password)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("connectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    string commandString = "spClientSelect";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            if (reader[3].ToString() == email)
                            {
                                if (reader[4].ToString() == password)
                                    return true;
                            }

                        }

                    }
                    catch { }
                }
            }

            return false;
        }

        static public bool VerifiyEmployee(string email, string password)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("connectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    string commandString = "spEmployeeSelect";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            if (reader[4].ToString() == email)
                            {
                                if (reader[3].ToString() == password)
                                    return true;
                            }
                        }
                    }
                    catch { }
                }
            }
            return false;
        }

        static public User GetUser(string email, string password)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("connectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    string commandString = "spClientSelect";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            if (reader[3].ToString() == email)
                            {
                                if (reader[4].ToString() == password)
                                {
                                    string str = reader[8].ToString();
                                    if (reader[8].ToString() == "True")
                                    {
                                        User user = new User(reader[0].ToString(), reader[1].ToString(), reader[2].ToString(), reader[3].ToString(), reader[4].ToString(), reader[5].ToString(), reader[6].ToString(), reader[7].ToString(), true);
                                        return user;
                                    }
                                    else
                                        return null;
                                }
                            }
                        }
                    }
                    catch { }
                }
                return null;
            }
        }

        static public Employee GetEmployee(string email, string password)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("connectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    string commandString = "spEmployeeSelect";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            if (reader[4].ToString() == email)
                            {
                                if (reader[3].ToString() == password)
                                {
                                    if (reader[10].ToString() == "True")
                                    {
                                        Employee employee = new Employee(reader[0].ToString(), reader[1].ToString(), reader[2].ToString(), reader[4].ToString(), reader[3].ToString(), reader[5].ToString(), reader[6].ToString(), reader[7].ToString(), reader[8].ToString(), bool.Parse(reader[9].ToString()), true);
                                        return employee;
                                    }
                                    else
                                        return null;
                                }
                            }
                        }
                    }
                    catch { }
                }
                return null;
            }
        }

        static public ObservableCollection<Product> GetProducts()
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("connectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    ObservableCollection<Product> productCollection = new ObservableCollection<Product>();
                    string commandString = "spProductSelect";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            Product product;

                            if (reader[5].ToString() == "True")
                            {
                            product = new Product(reader[0].ToString(), reader[1].ToString(), reader[2].ToString(), reader[3].ToString(), reader[4].ToString(), true);
                            }
                            else
                            {
                                product = new Product(reader[0].ToString(), reader[1].ToString(), reader[2].ToString(), reader[3].ToString(), reader[4].ToString(), false);
                            }

                            productCollection.Add(product);
                        }
                        return productCollection;
                    }
                    catch { }
                }
                return null;
            }
        }

        static public ObservableCollection<Product> GetProductsByType(string type, bool admin)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("connectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    ObservableCollection<Product> productCollection = new ObservableCollection<Product>();
                    string commandString = "spProductSelect";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            Product product;
                            if (reader[4].ToString() == type)
                            {
                                if (reader[5].ToString() == "True")
                                {
                                    product = new Product(reader[0].ToString(), reader[1].ToString(), reader[2].ToString(), reader[3].ToString(), reader[4].ToString(), true);
                                    productCollection.Add(product);
                                }
                                else
                                {
                                    if (admin)
                                    {
                                        product = new Product(reader[0].ToString(), reader[1].ToString(), reader[2].ToString(), reader[3].ToString(), reader[4].ToString(), false);
                                        productCollection.Add(product);
                                    }                             
                                }
                            }
                        }
                        return productCollection;
                    }
                    catch { }
                }
                return null;
            }
        }

        static public Product GetProductById(string id)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("connectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    ObservableCollection<Product> productCollection = new ObservableCollection<Product>();
                    string commandString = "spProductSelect";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            Product product;
                            if (reader[0].ToString() == id)
                            {
                                product = new Product(reader[0].ToString(), reader[1].ToString(), reader[2].ToString(), reader[3].ToString(), reader[4].ToString(), true);
                                return product;
                            }
                        }
                    }
                    catch { }
                }
                return null;
            }
        }

        static public string GetAllergensByProductId(int productId)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("connectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    string commandString = "spAllergenSelectFromProductId";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@productId", productId);
                        SqlDataReader reader = command.ExecuteReader();

                        string allergens = "";
                        while (reader.Read())
                        {
                            allergens += reader[0].ToString() + " ";
                        }
                        return allergens;
                    }
                    catch { }
                }
                return null;
            }
        }

        static public ObservableCollection<Menu> GetMenuCollection()
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("connectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    ObservableCollection<Menu> menuCollection = new ObservableCollection<Menu>();
                    string commandString = "spMenuSelect";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;

                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            Menu menu;
                            if (reader[2].ToString() == "True")
                            {
                                menu = new Menu(reader[0].ToString(), reader[1].ToString(), true);
                            }
                            else
                            {
                                menu = new Menu(reader[0].ToString(), reader[1].ToString(), false);
                            }
                            menuCollection.Add(menu);
                        }
                        return menuCollection;
                    }
                    catch { }
                }
                return null;
            }



        }

        static public ObservableCollection<Product> GetMenuProductsFromMenuId(string id)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("connectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    ObservableCollection<Product> productCollection = new ObservableCollection<Product>();
                    string commandString = "spMenuProductsFromIdSelect";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;

                        command.Parameters.AddWithValue("@id", int.Parse(id));

                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            Product product = Service.GetProductById(reader[0].ToString());
                            productCollection.Add(product);
                        }
                        return productCollection;
                    }
                    catch { }
                }
                return null;
            }
        }

        static public float GetMenuPriceFromId(string id)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("connectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    ObservableCollection<Product> productCollection = new ObservableCollection<Product>();
                    string commandString = "spMenuPriceFromIdSelect";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;

                        command.Parameters.AddWithValue("@id", int.Parse(id));

                        SqlDataReader reader = command.ExecuteReader();

                        string total = "";
                        if (reader.Read())
                        {
                            total = reader[0].ToString();
                        }

                        return float.Parse(total);

                    }
                    catch { }
                }
                return 0;
            }
        }

        static public void InsertDelivery(int clientId, float price, string payment, string telephone, string address)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("connectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    string commandString = "spDeliveryInsert";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@clientId", clientId);
                        command.Parameters.AddWithValue("@price", price);
                        command.Parameters.AddWithValue("@payment", payment);
                        command.Parameters.AddWithValue("@telephone", telephone);
                        command.Parameters.AddWithValue("@address", address);

                        command.ExecuteNonQuery();
                    }
                    catch (Exception e) { }
                }
            }
        }

        static public void InsertMenuDelivery(int menuId, int deliveryId, int quantity)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("connectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    string commandString = "spMenuDeliveryInsert";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@menuId", menuId);
                        command.Parameters.AddWithValue("@deliveryId", deliveryId);
                        command.Parameters.AddWithValue("@quantity", quantity);

                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        //log
                    }
                    catch (Exception e)
                    {
                        //logger
                    }
                }
            }
        }

        static public void InsertProductDelivery(int productId, int deliveryId, int quantity)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("connectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    string commandString = "spProductDeliveryInsert";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@productId", productId);
                        command.Parameters.AddWithValue("@deliveryId", deliveryId);
                        command.Parameters.AddWithValue("@quantity", quantity);

                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        //log
                    }
                    catch (Exception e)
                    {
                        //logger
                    }
                }
            }
        }


        static public int LastDeliveryIdFromClientIdSelect(string clientId)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("connectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    ObservableCollection<Product> productCollection = new ObservableCollection<Product>();
                    string commandString = "spLastDeliveryIdFromClientIdSelect";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;

                        command.Parameters.AddWithValue("@clientId", int.Parse(clientId));

                        SqlDataReader reader = command.ExecuteReader();


                        if (reader.Read())
                        {
                            string a = reader[0].ToString();
                            return reader.GetInt32(0);
                        }
                    }
                    catch { }
                }
                return 0;
            }
        }


        static public void InsertEmployee(string firstName, string lastName, string password, string email, string address, string city, string telephone, string salary, bool admin, bool active)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("connectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    string commandString = "spEmployeeInsert";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@first_name", firstName);
                        command.Parameters.AddWithValue("@last_name", lastName);
                        command.Parameters.AddWithValue("@password", password);
                        command.Parameters.AddWithValue("@email", email);
                        command.Parameters.AddWithValue("@address", address);
                        command.Parameters.AddWithValue("@city", city);
                        command.Parameters.AddWithValue("@telephone", telephone);
                        command.Parameters.AddWithValue("@salary", float.Parse(salary));
                        command.Parameters.AddWithValue("@admin", admin);
                        command.Parameters.AddWithValue("@active", active);
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        //log
                    }
                    catch (Exception e)
                    {
                        //logger
                    }
                }
            }
        }

        static public string CorrectInputEmployee(string firstName, string lastName, string email, string password, string address, string city, string telephone, string salary, string admin, string active)
        {
            if (firstName == "")
                return "First name cannot be empty !";

            if (lastName == "")
                return "Last name cannot be empty !";

            if (telephone.Length != 10)
                return "Invalid phone number";
            if (EmailAndTelephoneVerifyEmployee(email, telephone) == "telephone")
                return "Telephone number already exists !";

            if (email == "" || !email.Contains("@"))
                return "Invalid email address !";
            if (EmailAndTelephoneVerifyEmployee(email, telephone) == "email")
                return "Email address already exists !";

            if (address == "")
                return "Address must not be empty";

            if (city == "")
                return "City must not be empty";

            if (password.Length < 6)
                return "Password must be longer than 6 characters !";

            if (salary == "")
                return "Salary field is empty !";

            if (admin == "")
                return "Admin field is empty !";

            if (active == "")
                return "Active field is empty !";

            return "Ok";
        }

        static public void InsertClient(string firstName, string lastName, string email, string password, string address, string city, string telephone)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("connectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    string commandString = "spClientInsert";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        command.Parameters.AddWithValue("@first_name", firstName);
                        command.Parameters.AddWithValue("@last_name", lastName);
                        command.Parameters.AddWithValue("@email", email);
                        command.Parameters.AddWithValue("@password", password);
                        command.Parameters.AddWithValue("@address", address);
                        command.Parameters.AddWithValue("@city", city);
                        command.Parameters.AddWithValue("@telephone", telephone);
                        command.Parameters.AddWithValue("@active", true);
                        command.ExecuteNonQuery();
                    }
                    catch (SqlException e)
                    {
                        //log
                    }
                    catch (Exception e)
                    {
                        //logger
                    }
                }
            }

        }
       
        static public string CorrectInput(string firstName, string lastName, string email, string password, string address, string city, string telephone)
        {
            if (firstName == "")
                return "First name cannot be empty !";

            if (lastName == "")
                return "Last name cannot be empty !";

            if (telephone.Length != 10)
                return "Invalid phone number";
            if (EmailAndTelephoneVerify(email, telephone) == "telephone")
                return "Telephone number already exists !";

            if (email == "" || !email.Contains("@"))
                return "Invalid email address !";
            if (EmailAndTelephoneVerify(email, telephone) == "email")
                return "Email address already exists !";

            if (address == "")
                return "Address must not be empty";

            if (city == "")
                return "City must not be empty";

            if (password.Length < 6)
                return "Password must be longer than 6 characters !";

            return "Ok";
        }

        static public string EmailAndTelephoneVerify(string email, string telephone)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("connectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    string commandString = "spClientSelect";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            if (reader[3].ToString() == email)
                                return "email";
                            if (reader[7].ToString() == telephone)
                                return "telephone";
                        }
                    }
                    catch { }
                }
            }
            return "Ok";
        }

        static public string EmailAndTelephoneVerifyEmployee(string email, string telephone)
        {
            using (SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings.Get("connectionString")))
            {
                connection.Open();
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    string commandString = "spEmployeeSelect";
                    try
                    {
                        SqlCommand command = new SqlCommand(commandString, connection);
                        command.CommandType = System.Data.CommandType.StoredProcedure;
                        SqlDataReader reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            if (reader[4].ToString() == email)
                                return "email";
                            if (reader[7].ToString() == telephone)
                                return "telephone";
                        }
                    }
                    catch { }
                }
            }
            return "Ok";
        }
    }
}
