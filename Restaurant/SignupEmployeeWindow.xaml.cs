﻿using System.Windows;


namespace Restaurant
{
    /// <summary>
    /// Interaction logic for SingupWindow.xaml
    /// </summary>
    public partial class SignupEmployeeWindow : Window
    {
        public SignupEmployeeWindow()
        {
            InitializeComponent();
        }

        private void btn_SignUp_Click(object sender, RoutedEventArgs e)
        {
            string inputOk = Service.CorrectInputEmployee(txt_firstName.Text, txt_lastName.Text, txt_email.Text, passwordBox.Password, txt_address.Text, txt_city.Text, txt_telephone.Text, txt_salary.Text, comboBox_admin.Text, comboBox_active.Text); ;
            if (inputOk=="Ok")
            {
                Service.InsertEmployee(txt_firstName.Text, txt_lastName.Text, passwordBox.Password, txt_email.Text, txt_address.Text, txt_city.Text, txt_telephone.Text, txt_salary.Text, bool.Parse(comboBox_admin.Text), bool.Parse(comboBox_active.Text));
                PopupWindow popUp = new PopupWindow("Account created successfully !");
                popUp.Show();
                this.Close();
            }
            else
            {
                txtBlock_error.Text = inputOk;
            }
        }
    }
}
