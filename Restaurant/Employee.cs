﻿namespace Restaurant
{
    public class Employee
    {
        private string id, firstName, lastName, email, password, address, city, telephone, salary;
        private bool admin;
        private bool active;

        public Employee(string id, string firstName, string lastName, string email, string password, string address, string city, string telephone, string salary, bool admin, bool active)
        {
            Id = id;
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Password = password;
            Address = address;
            City = city;
            Telephone = telephone;
            Salary = salary;
            Admin = admin;
            Active = active;
        }

        public string Id { get => id; set => id = value; }
        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public string Email { get => email; set => email = value; }
        public string Password { get => password; set => password = value; }
        public string Address { get => address; set => address = value; }
        public string City { get => city; set => city = value; }
        public string Telephone { get => telephone; set => telephone = value; }
        public string Salary { get => salary; set => salary = value; }
        public bool Admin { get => admin; set => admin = value; }
        public bool Active { get => active; set => active = value; }
    }
}
