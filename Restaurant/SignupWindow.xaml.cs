﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Restaurant
{
    /// <summary>
    /// Interaction logic for SingupWindow.xaml
    /// </summary>
    public partial class SignupWindow : Window
    {
        public SignupWindow()
        {
            InitializeComponent();
        }

        private void btn_SignUp_Click(object sender, RoutedEventArgs e)
        {
            string inputOk = Service.CorrectInput(txt_firstName.Text, txt_lastName.Text, txt_email.Text, passwordBox.Password, txt_address.Text, txt_city.Text, txt_telephone.Text);
            if (inputOk=="Ok")
            {
                Service.InsertClient(txt_firstName.Text, txt_lastName.Text, txt_email.Text, passwordBox.Password, txt_address.Text, txt_city.Text, txt_telephone.Text);
                PopupWindow popUp = new PopupWindow("Account created successfully !");
                popUp.Show();
                this.Close();
            }
            else
            {
                txtBlock_error.Text = inputOk;
            }
        }
    }
}
