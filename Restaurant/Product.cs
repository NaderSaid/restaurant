﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant
{
    public class Product
    {
        string id, name, price, quantity, foodType, allergens;
        
        bool active;
        public Product(string id, string name, string price, string quantity, string foodType, bool active)
        {
            this.Id = id;
            this.Name = name;
            this.Price = price;
            this.Quantity = quantity;
            this.FoodType = foodType;
            this.Active = active;
            this.Allergens = Service.GetAllergensByProductId(int.Parse(id));
        }       

        public string Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Price { get => price; set => price = value; }
        public string Quantity { get => quantity; set => quantity = value; }
        public string FoodType { get => foodType; set => foodType = value; }
        public bool Active { get => active; set => active = value; }
        public string Allergens { get => allergens; set => allergens = value; }
    }
}
