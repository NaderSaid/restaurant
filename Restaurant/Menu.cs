﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Restaurant
{
    public class Menu
    {
        private string name, id;
        float price;
        bool active;
        ObservableCollection<Product> menuProductCollection;

        public Menu(string id, string name, bool active)
        {
            this.Id = id;
            this.Name = name;
            this.Active = active;
            this.MenuProductCollection = Service.GetMenuProductsFromMenuId(id);
            this.price = Service.GetMenuPriceFromId(id);
        }

        public string Name { get => name; set => name = value; }
        public string Id { get => id; set => id = value; }
        public bool Active { get => active; set => active = value; }
        public ObservableCollection<Product> MenuProductCollection { get => menuProductCollection; set => menuProductCollection = value; }
        public float Price { get => price; set => price = value; }
    }
}
