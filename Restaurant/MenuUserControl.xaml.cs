﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;

namespace Restaurant
{
    /// <summary>
    /// Interaction logic for PizzaUserControl.xaml
    /// </summary>
    public partial class MenuUserControl : UserControl
    {
        private ObservableCollection<Menu> menuCollection;
        public User user;
        public bool employee;

        public MenuUserControl()
        {
            InitializeComponent();
            menuCollection = Service.GetMenuCollection();        
        }

        public void Update()
        {
            menuCollection = Service.GetMenuCollection();
            foreach (Menu menu in menuCollection)
            {
                if (!employee)
                {
                    if (menu.Active == true)
                        comboBox_menu.Items.Add(menu.Name);
                }
                else
                {
                    comboBox_menu.Items.Add(menu.Name);
                }
            }
        }

        private void DataGrid_OnAutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (!employee)
            {
                if (e.PropertyName == "Id" || e.PropertyName == "Active")
                {
                    e.Column = null;
                }
            }
        }

        private void comboBox_menu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            string selectedName = comboBox_menu.SelectedItem.ToString();

            foreach (Menu menu in this.menuCollection)
            {
                if (menu.Name == selectedName)
                {
                    dataGrid_Menus.ItemsSource = menu.MenuProductCollection;
                    break;
                }
            }
        }

        private void btn_addToBasket_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (this.user != null)
            {
                if (comboBox_menu.SelectedItem != null)
                {
                    foreach (Menu menu in menuCollection)
                    {
                        if (comboBox_menu.SelectedItem.ToString() == menu.Name)
                        {                          
                            MainShopWindow parentWindow = (MainShopWindow)Window.GetWindow(this);
                            parentWindow.AddToBasketMenu(menu);
                            PopupWindow popUp = new PopupWindow("Menu added to basket !");
                            popUp.Show();
                            break;
                        }
                    }
                }
                else
                {
                    PopupWindow popUp = new PopupWindow("Menu added to basket !");
                    popUp.Show();
                }
            }
            else
            {
                PopupWindow popUp = new PopupWindow("You must create an account to order food.\nGo to the basket tab to create an account.");
                popUp.Show();
            }
} 
    }
}
