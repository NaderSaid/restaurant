﻿namespace Restaurant
{
    public class User
    {
        private string id, firstName, lastName, email, password, address, city, telephone;
        private bool active;

        public string Id { get => id; set => id = value; }
        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public string Email { get => email; set => email = value; }
        public string Password { get => password; set => password = value; }
        public string Address { get => address; set => address = value; }
        public string City { get => city; set => city = value; }
        public string Telephone { get => telephone; set => telephone = value; }
        public bool Active { get => active; set => active = value; }

        public User(string id, string firstName, string lastName, string email, string password, string address, string city, string telephone, bool active)
        {
            this.Id = id;
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Email = email;
            this.Password = password;
            this.Address = address;
            this.City = city;
            this.Telephone = telephone;
            this.Active = active;
        }
    } 
}
