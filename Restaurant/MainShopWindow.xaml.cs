﻿using System.Collections.ObjectModel;
using System.Windows;


namespace Restaurant
{

    public partial class MainShopWindow : Window
    {
        public User user;
        public Employee employee;
        private ObservableCollection<Product> basketProducts = new ObservableCollection<Product>();
        private ObservableCollection<Menu> basketMenus = new ObservableCollection<Menu>();


        public MainShopWindow(User user)
        {
            InitializeComponent();
            this.user = user;
            uc_burger.user = user; 
            
            uc_buy.btn_CreateAccount.Visibility = Visibility.Collapsed;
            uc_buy.btn_CreateEmployeeAccount.Visibility = Visibility.Collapsed;
            uc_buy.btn_LogIn.Visibility = Visibility.Collapsed;
            uc_buy.txtblock_Slash.Visibility = Visibility.Collapsed;
            
            uc_desert.user = user;
            uc_pasta.user = user;
            uc_pizza.user = user;
            uc_menu.user = user;
            uc_salad.user = user;
            uc_soup.user = user;
            UpdateAllControls();
        }

        public MainShopWindow(Employee employee)
        {
            InitializeComponent();
            this.employee = employee;

            uc_buy.btn_CreateAccount.Visibility = Visibility.Collapsed;
            if (!employee.Admin)
            { 
                uc_buy.btn_CreateEmployeeAccount.Visibility = Visibility.Collapsed;
            }
            uc_buy.btn_LogIn.Visibility = Visibility.Collapsed;
            uc_buy.txtblock_Slash.Visibility = Visibility.Collapsed;
            

            uc_burger.employee = true;
            uc_desert.employee = true;
            uc_menu.employee = true;
            uc_pasta.employee = true;
            uc_pizza.employee = true;
            uc_salad.employee = true;
            uc_soup.employee = true;
            UpdateAllControls();
        }

        public MainShopWindow()
        {
            InitializeComponent();
            this.user = null;
            this.employee = null;

            uc_buy.btn_CreateEmployeeAccount.Visibility = Visibility.Collapsed;
            uc_buy.btn_CheckOut.Visibility = Visibility.Collapsed;
            UpdateAllControls();
        }

        public void UpdateAllControls()
        {
            uc_burger.Update();
            uc_desert.Update();
            uc_menu.Update();
            uc_pasta.Update();
            uc_pizza.Update();
            uc_salad.Update();
            uc_soup.Update();
        }

        public void AddToBasketProduct(Product product)
        {
            basketProducts.Add(product);
            uc_buy.UpdateDataGrid(basketProducts, basketMenus);     
        }

        public void RemoveBasketProduct(string productName)
        {
            foreach (Product product in basketProducts)
            {
                if (product.Name == productName)
                {
                    basketProducts.Remove(product);
                    break;
                }
            }
        }

        public void AddToBasketMenu(Menu menu)
        {
            basketMenus.Add(menu);
            uc_buy.UpdateDataGrid(basketProducts, basketMenus);
        }

        public void RemoveBasketMenu(string menuName)
        {
            foreach (Menu menu in basketMenus)
            {
                if (menu.Name == menuName)
                {
                    basketMenus.Remove(menu);
                    break;
                }
            }
        }

        public void CheckOut(float totalPrice)
        {
            CheckOutWindow checkOutWindow = new CheckOutWindow(user, totalPrice, basketProducts, basketMenus);
            checkOutWindow.Show();
            this.Close();
        }

        private void btn_basket_Click(object sender, RoutedEventArgs e)
        {
            rectangle_panel.Arrange(new Rect(700, 0, 100, 100));

            uc_menu.Visibility = Visibility.Hidden;
            uc_soup.Visibility = Visibility.Hidden;
            uc_burger.Visibility = Visibility.Hidden;
            uc_pasta.Visibility = Visibility.Hidden;
            uc_pizza.Visibility = Visibility.Hidden;
            uc_salad.Visibility = Visibility.Hidden;
            uc_desert.Visibility = Visibility.Hidden;
            uc_buy.Visibility = 0;

        }

        private void btn_burger_Click(object sender, RoutedEventArgs e)
        {
            rectangle_panel.Arrange(new Rect(200, 0, 100, 100));

            uc_menu.Visibility = Visibility.Hidden;
            uc_soup.Visibility = Visibility.Hidden;
            uc_burger.Visibility = 0;
            uc_pasta.Visibility = Visibility.Hidden;
            uc_pizza.Visibility = Visibility.Hidden;
            uc_salad.Visibility = Visibility.Hidden;
            uc_desert.Visibility = Visibility.Hidden;
            uc_buy.Visibility = Visibility.Hidden;
        }

        private void btn_desert_Click(object sender, RoutedEventArgs e)
        {
            rectangle_panel.Arrange(new Rect(600, 0, 100, 100));

            uc_menu.Visibility = Visibility.Hidden;
            uc_soup.Visibility = Visibility.Hidden;
            uc_burger.Visibility = Visibility.Hidden;
            uc_pasta.Visibility = Visibility.Hidden;
            uc_pizza.Visibility = Visibility.Hidden;
            uc_salad.Visibility = Visibility.Hidden;
            uc_desert.Visibility = 0;
            uc_buy.Visibility = Visibility.Hidden;
        }

        private void btn_menu_Click(object sender, RoutedEventArgs e)
        {
            rectangle_panel.Arrange(new Rect(0, 0, 100, 100));
            uc_menu.Visibility = 0;
            uc_soup.Visibility = Visibility.Hidden;
            uc_burger.Visibility = Visibility.Hidden;
            uc_pasta.Visibility = Visibility.Hidden;
            uc_pizza.Visibility = Visibility.Hidden;
            uc_salad.Visibility = Visibility.Hidden;
            uc_desert.Visibility = Visibility.Hidden;
            uc_buy.Visibility = Visibility.Hidden;
        }

        private void btn_pasta_Click(object sender, RoutedEventArgs e)
        {
            rectangle_panel.Arrange(new Rect(300, 0, 100, 100));

            uc_menu.Visibility = Visibility.Hidden;
            uc_soup.Visibility = Visibility.Hidden;
            uc_burger.Visibility = Visibility.Hidden;
            uc_pasta.Visibility = 0;
            uc_pizza.Visibility = Visibility.Hidden;
            uc_salad.Visibility = Visibility.Hidden;
            uc_desert.Visibility = Visibility.Hidden;
            uc_buy.Visibility = Visibility.Hidden;
        }

        private void btn_pizza_Click(object sender, RoutedEventArgs e)
        {
            rectangle_panel.Arrange(new Rect(400, 0, 100, 100));

            uc_menu.Visibility = Visibility.Hidden;
            uc_soup.Visibility = Visibility.Hidden;
            uc_burger.Visibility = Visibility.Hidden;
            uc_pasta.Visibility = Visibility.Hidden;
            uc_pizza.Visibility = 0;
            uc_salad.Visibility = Visibility.Hidden;
            uc_desert.Visibility = Visibility.Hidden;
            uc_buy.Visibility = Visibility.Hidden;
        }

        private void btn_salad_Click(object sender, RoutedEventArgs e)
        {
            rectangle_panel.Arrange(new Rect(500, 0, 100, 100));

            uc_menu.Visibility = Visibility.Hidden;
            uc_soup.Visibility = Visibility.Hidden;
            uc_burger.Visibility = Visibility.Hidden;
            uc_pasta.Visibility = Visibility.Hidden;
            uc_pizza.Visibility = Visibility.Hidden;
            uc_salad.Visibility = 0;
            uc_desert.Visibility = Visibility.Hidden;
            uc_buy.Visibility = Visibility.Hidden;
        }

        private void btn_soup_Click(object sender, RoutedEventArgs e)
        {
            rectangle_panel.Arrange(new Rect(100, 0, 100, 100));

            uc_menu.Visibility = Visibility.Hidden;
            uc_soup.Visibility = 0;
            uc_burger.Visibility = Visibility.Hidden;
            uc_pasta.Visibility = Visibility.Hidden;
            uc_pizza.Visibility = Visibility.Hidden;
            uc_salad.Visibility = Visibility.Hidden;
            uc_desert.Visibility = Visibility.Hidden;
            uc_buy.Visibility = Visibility.Hidden;
        }
    }
}
