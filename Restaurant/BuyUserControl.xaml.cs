﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;

namespace Restaurant
{
    public partial class BuyUserControl : UserControl
    {    
        public float totalPrice;

        public BuyUserControl()
        {
            InitializeComponent();         
        }

        public void UpdateDataGrid(ObservableCollection<Product> productCollection, ObservableCollection<Menu> menuCollection)
        {
            dataGrid_Products.ItemsSource = productCollection;
            dataGrid_Menus.ItemsSource = menuCollection;
            UpdateTotalPrice(productCollection, menuCollection);
        }

        public void UpdateTotalPrice(ObservableCollection<Product> productCollection, ObservableCollection<Menu> menuCollection)
        {
            float totalPriceAux = 0;
            foreach (Product product in productCollection)
            {
                totalPriceAux += float.Parse(product.Price);
            }

            foreach (Menu menu in menuCollection)
            {
                totalPriceAux += menu.Price;
            }

            totalPrice = totalPriceAux;
            txtblock_TotalPrice.Text = totalPrice.ToString() + " RON";
        }

        private void DataGrid_OnAutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (e.PropertyName == "Id" || e.PropertyName == "Active" || e.PropertyName == "FoodType" || e.PropertyName == "MenuProductCollection")
            {
                e.Column = null;
            }
        }

        private void btn_CreateAccount_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            SignupWindow signupWindow = new SignupWindow();
            signupWindow.Show();
        }

        private void btn_CreateEmployeeAccount_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            SignupEmployeeWindow signupEmployeeWindow = new SignupEmployeeWindow();
            signupEmployeeWindow.Show();
        }

        private void btn_LogIn_Click(object sender, System.Windows.RoutedEventArgs e)
        {

            MainWindow mainWindow = new MainWindow();
            mainWindow.Show();

            MainShopWindow parentWindow = (MainShopWindow)Window.GetWindow(this);
            parentWindow.Close();
        }

        private void btn_RemoveItem_Click(object sender, RoutedEventArgs e)
        {
            if (dataGrid_Menus.SelectedItem != null)
            {
                Menu menu = (Menu) dataGrid_Menus.SelectedItem;

                MainShopWindow parentWindow = (MainShopWindow)Window.GetWindow(this);
                parentWindow.RemoveBasketMenu(menu.Name);
            }

            if (dataGrid_Products.SelectedItem != null)
            {
                Product product = (Product)dataGrid_Products.SelectedItem;

                MainShopWindow parentWindow = (MainShopWindow)Window.GetWindow(this);
                parentWindow.RemoveBasketProduct(product.Name);
            }
        }

        private void btn_CheckOut_Click(object sender, RoutedEventArgs e)
        {
            if (totalPrice != 0)
            {
                MainShopWindow parentWindow = (MainShopWindow)Window.GetWindow(this);
                parentWindow.CheckOut(totalPrice);
            }
        }
    }
}
