﻿using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;

namespace Restaurant
{
    /// <summary>
    /// Interaction logic for PizzaUserControl.xaml
    /// </summary>
    public partial class PizzaUserControl : UserControl
    {
        public User user;
        public bool employee;

        public PizzaUserControl()
        {
            InitializeComponent();
            ObservableCollection<Product> productCollection = Service.GetProductsByType("Pizza", employee);
            dataGrid.ItemsSource = productCollection;
    
            //dataGrid.Columns.Remove(dataGrid.Columns.ElementAt(0));
        }

        public void Update()
        {
            ObservableCollection<Product> productCollection = Service.GetProductsByType("Pizza", employee);
            dataGrid.ItemsSource = productCollection;
        }

        private void DataGrid_OnAutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
            if (!employee)
            {
                if (e.PropertyName == "Id" || e.PropertyName == "Active" || e.PropertyName == "FoodType")
                {
                    e.Column = null;
                }
            }
        }
        private void btn_addToBasket_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (this.user != null)
            {
                var rowIndex = dataGrid.SelectedIndex;
                var row = (DataGridRow)dataGrid.ItemContainerGenerator.ContainerFromIndex(rowIndex);
                MainShopWindow parentWindow = (MainShopWindow)Window.GetWindow(this);
                Product product = (Product)row.Item;
                parentWindow.AddToBasketProduct(product);
            }
            else
            {
                PopupWindow popupWindow = new PopupWindow("You must create an account to order food.\nGo to the basket tab to create an account.");
                popupWindow.Show();
            }
        }
    }
}
