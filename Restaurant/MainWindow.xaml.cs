﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Restaurant
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void btn_Signup_Click(object sender, RoutedEventArgs e)
        {
            SignupWindow singupWindow = new SignupWindow();
            singupWindow.Show();
        }

        private void btn_Login_Click(object sender, RoutedEventArgs e)
        {
            if (Service.VerifyUser(txt_email.Text, passwordBox_one.Password))
            {
                if (Service.GetUser(txt_email.Text, passwordBox_one.Password) != null)
                {
                    User user = Service.GetUser(txt_email.Text, passwordBox_one.Password);

                    MainShopWindow mainShopWindow = new MainShopWindow(user);
                    mainShopWindow.Show();
                    this.Close();
                }
                else
                {
                    PopupWindow popUp = new PopupWindow("Email or password is incorrect !");
                    popUp.Show();
                }
            }
            else
            {
                if (Service.VerifiyEmployee(txt_email.Text, passwordBox_one.Password))
                {
                    Employee employee = Service.GetEmployee(txt_email.Text, passwordBox_one.Password);

                    MainShopWindow mainShopWindow = new MainShopWindow(employee);
                    mainShopWindow.Show();
                    this.Close();
                }
                else
                {
                    PopupWindow popupWindow = new PopupWindow("Email or password is incorrect !");
                    popupWindow.Show();
                }
            }
        }

        private void btn_Continue_Click(object sender, RoutedEventArgs e)
        {
            MainShopWindow mainShopWindow = new MainShopWindow();
            mainShopWindow.Show();
            this.Close();
        }
    }
}
